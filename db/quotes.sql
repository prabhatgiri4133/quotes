-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2017 at 04:08 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quotes`
--

DELIMITER $$
--
-- Procedures
--
CREATE PROCEDURE `sp_InsertGroupPermission` (IN `dataArr` TEXT, IN `GroupId` INT(11), IN `UserId` INT(11))  BEGIN
	DECLARE v_cur_position INT; 
	DECLARE v_remainder TEXT; 
	DECLARE v_cur_string VARCHAR(255); 
	DECLARE ModuleId INT(11);
	DECLARE UserActionId INT(11);
	SET v_remainder = dataArr; 
	SET v_cur_position = 1;
	
	
	DELETE FROM permissions_per_group 
	WHERE group_id = GroupId;
	  	    
	    
	    WHILE CHAR_LENGTH(v_remainder) > 0 AND v_cur_position > 0 DO
		SET v_cur_position = INSTR(v_remainder, ','); 
		IF v_cur_position = 0 THEN 
		    SET v_cur_string = v_remainder; 
		ELSE 
		    SET v_cur_string = LEFT(v_remainder, v_cur_position - 1); 
		END IF; 
		IF TRIM(v_cur_string) != '' THEN 		    
		    SET ModuleId = 
				REPLACE(SUBSTRING(SUBSTRING_INDEX(v_cur_string, '_', 1),
				LENGTH(SUBSTRING_INDEX(v_cur_string, '_', 1 -1)) + 1),
				'_', '');
				
		    SET UserActionId = 
				REPLACE(SUBSTRING(SUBSTRING_INDEX(v_cur_string, '_', 2),
				LENGTH(SUBSTRING_INDEX(v_cur_string, '_', 2-1)) + 1),
				'_', '');
				
		    INSERT INTO permissions_per_group
			(module_id,user_action_id,group_id,added_by,added_date,modified_by,modified_date)
		    VALUES 
			(ModuleId, UserActionId, GroupId, UserId, CURDATE(), UserId, CURDATE());                 
		END IF; 
		SET v_remainder = SUBSTRING(v_remainder, v_cur_position + 1); 
	    END WHILE; 
	
    END$$

CREATE PROCEDURE `sp_InsertUserPermission` (IN `dataArr` TEXT, IN `ForUserId` INT(11), IN `UserId` INT(11))  BEGIN
	DECLARE v_cur_position INT; 
	DECLARE v_remainder TEXT; 
	DECLARE v_cur_string VARCHAR(255); 
	DECLARE ModuleId INT(11);
	DECLARE UserActionId INT(11);
	SET v_remainder = dataArr; 
	SET v_cur_position = 1;
	
	
	DELETE FROM permissions_per_user 
	WHERE user_id = ForUserId;
	  	    
	    
	    WHILE CHAR_LENGTH(v_remainder) > 0 AND v_cur_position > 0 DO
		SET v_cur_position = INSTR(v_remainder, ','); 
		IF v_cur_position = 0 THEN 
		    SET v_cur_string = v_remainder; 
		ELSE 
		    SET v_cur_string = LEFT(v_remainder, v_cur_position - 1); 
		END IF; 
		IF TRIM(v_cur_string) != '' THEN 		    
		    SET ModuleId = 
				REPLACE(SUBSTRING(SUBSTRING_INDEX(v_cur_string, '_', 1),
				LENGTH(SUBSTRING_INDEX(v_cur_string, '_', 1 -1)) + 1),
				'_', '');
				
		    SET UserActionId = 
				REPLACE(SUBSTRING(SUBSTRING_INDEX(v_cur_string, '_', 2),
				LENGTH(SUBSTRING_INDEX(v_cur_string, '_', 2-1)) + 1),
				'_', '');
				
		    INSERT INTO permissions_per_user
			(module_id,user_action_id,user_id,added_by,added_date,modified_by,modified_date)
		    VALUES 
			(ModuleId, UserActionId, ForUserId, UserId, CURDATE(), UserId, CURDATE());                 
		END IF; 
		SET v_remainder = SUBSTRING(v_remainder, v_cur_position + 1); 
	    END WHILE; 
	
    END$$

--
-- Functions
--
CREATE FUNCTION `fn_CheckGroupPermission` (`ModuleId` INT(11), `UserActionId` INT(11), `GroupId` INT(11)) RETURNS TINYINT(1) BEGIN
	DECLARE bReturn BOOL;
	IF GroupId = 0 THEN
		RETURN TRUE;
	END IF;
	IF EXISTS 
		(
		 SELECT permission_per_group_id 
		 FROM permissions_per_group
		 WHERE module_id = ModuleId
		 AND user_action_id = UserActionId
		 AND group_id = GroupId
		 ) 
		
		THEN
		RETURN TRUE;
	END IF;
	RETURN FALSE;
    END$$

CREATE FUNCTION `fn_CheckMenuPermission` (`MenuId` INT(11), `LoginId` INT(11)) RETURNS TINYINT(1) BEGIN
	DECLARE bReturn BOOL;
	DECLARE GroupId INT(11);	
	SELECT user_group INTO GroupId FROM users  WHERE userid = LoginId;
	IF GroupId = 0 THEN RETURN 1; END IF;
	
	IF EXISTS 
		(
			 SELECT permission_per_user_id 
			 FROM permissions_per_user
			 WHERE module_id = MenuId
			 AND user_id = LoginId LIMIT 0,1
		 ) 
		THEN
		RETURN 1;
	ELSEIF EXISTS
			(
				 SELECT permission_per_group_id 
				 FROM permissions_per_group
				 WHERE module_id = MenuId
				 AND group_id = GroupId LIMIT 0,1
			 ) 
		THEN
		RETURN 1;
	
	END IF;
	
	RETURN 0;
    END$$

CREATE FUNCTION `fn_CheckPermissionByLoginId` (`ModuleCode` VARCHAR(20), `UserActionCode` VARCHAR(20), `LoginId` INT(11)) RETURNS TINYINT(1) BEGIN
	DECLARE bReturn BOOL;
	DECLARE GroupId INT(11);
	DECLARE ModuleId INT(11);
	DECLARE UserActionId INT(11);
		
	SELECT fn_GetModuleId(ModuleCode) INTO ModuleId;
	SELECT fn_GetUserActionId(UserActionCode) INTO UserActionId;
	SELECT user_group INTO GroupId FROM users  WHERE userid = LoginId;
	IF fn_CheckUserPermission(ModuleId, UserActionId, LoginId) = 1  THEN
		RETURN TRUE;
	ELSEIF fn_CheckGroupPermission(ModuleId, UserActionId, GroupId) = 1 THEN
		RETURN TRUE;
	END IF;
	RETURN FALSE;
    END$$

CREATE FUNCTION `fn_CheckUserPermission` (`ModuleId` INT(11), `UserActionId` INT(11), `UserId` INT(11)) RETURNS TINYINT(1) BEGIN
	DECLARE bReturn BOOL;
	IF EXISTS 
		(
		 SELECT permission_per_user_id 
		 FROM permissions_per_user
		 WHERE module_id = ModuleId
		 AND user_action_id = UserActionId
		 AND user_id = UserId
		 ) 
		
		THEN
		RETURN TRUE;
	END IF;
	RETURN FALSE;
    END$$

CREATE FUNCTION `fn_GetModuleId` (`ModuleCode` VARCHAR(20)) RETURNS INT(11) BEGIN
	DECLARE iReturn INT(11);
	SET iReturn = 0;
	SELECT menuid INTO iReturn FROM admin_menu WHERE module_code = ModuleCode;
	RETURN iReturn;
    END$$

CREATE FUNCTION `fn_GetUserActionId` (`UserActionCode` VARCHAR(20)) RETURNS INT(11) BEGIN
	DECLARE iReturn INT(11);
	SET iReturn = 0;
	SELECT user_action_id INTO iReturn FROM user_actions WHERE user_action_code = UserActionCode;
	RETURN iReturn;
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `menuid` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_link` varchar(255) NOT NULL,
  `group_label` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `module_code` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `position` tinyint(4) NOT NULL,
  `icon_class` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`menuid`, `parent_id`, `menu_name`, `menu_link`, `group_label`, `status`, `module_code`, `description`, `position`, `icon_class`) VALUES
(1, 0, 'Manage', 'Quote', '', 1, NULL, '', 0, 'zmdi zmdi-collection-item'),
(2, 1, 'Quote', 'Quote/ListAll', '', 1, 'QUOTE', '', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `groupid` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `status` tinyint(2) DEFAULT '0',
  `company` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`groupid`, `group_name`, `status`, `company`) VALUES
(1, 'Company Admin', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permid` int(11) NOT NULL,
  `menuid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permissions_per_group`
--

CREATE TABLE `permissions_per_group` (
  `permission_per_group_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_action_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions_per_group`
--

INSERT INTO `permissions_per_group` (`permission_per_group_id`, `module_id`, `user_action_id`, `group_id`, `added_by`, `added_date`, `modified_by`, `modified_date`) VALUES
(14, 1, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(15, 1, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(16, 1, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(17, 1, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(18, 1, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(19, 2, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(20, 2, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(21, 2, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(22, 2, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(23, 3, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(24, 3, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(25, 3, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(26, 3, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(27, 2, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(28, 1, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(29, 1, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(30, 1, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(31, 1, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(32, 2, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(33, 2, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(34, 2, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(35, 2, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(36, 3, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(37, 3, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(38, 3, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(39, 3, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(40, 3, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(41, 1, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(42, 1, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(43, 1, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(44, 1, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(45, 2, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(46, 2, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(47, 2, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(48, 2, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(49, 3, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(50, 3, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(51, 3, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(52, 3, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permissions_per_user`
--

CREATE TABLE `permissions_per_user` (
  `permission_per_user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions_per_user`
--

INSERT INTO `permissions_per_user` (`permission_per_user_id`, `module_id`, `user_action_id`, `user_id`, `added_by`, `added_date`, `modified_by`, `modified_date`) VALUES
(1, 1, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(2, 2, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(3, 2, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(4, 2, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(5, 2, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(6, 3, 1, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(7, 3, 2, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(8, 3, 3, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00'),
(9, 3, 4, 1, 0, '2017-04-19 00:00:00', 0, '2017-04-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` int(11) NOT NULL,
  `quote` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `background` varchar(255) NOT NULL,
  `font_color` varchar(40) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('enabled','disabled','deleted') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `quote`, `author`, `background`, `font_color`, `created_by`, `created_on`, `status`) VALUES
(1, 'I am groot', 'groot', 'green', 'black', 1, '2017-10-01 10:57:23', 'enabled'),
(2, 'penguin pengiun', 'linux', 'black', 'red', 1, '2017-10-01 10:58:48', 'enabled'),
(3, 'a', 'alpha', 'a', 'a', 1, '2017-10-01 16:43:50', 'deleted');

-- --------------------------------------------------------

--
-- Table structure for table `userkeys`
--

CREATE TABLE `userkeys` (
  `id` int(11) NOT NULL,
  `firebasetoken` varchar(255) NOT NULL,
  `serialnumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userkeys`
--

INSERT INTO `userkeys` (`id`, `firebasetoken`, `serialnumber`) VALUES
(1, 'fj2RVhbw3xo:APA91bGoaqMvtA1Hd2J53ozI0b-rR-LLlc6P2Xc5_o7uWYtMvaUbyuQINEF_t5LqnJZrfhg3MbD1T4g9dtxTFfd6RCGN6E7KBjykQw4JGBfCm_IFEw-EYwjGPr6-TswWmOjV6ztWOBX_', '12345678');

-- --------------------------------------------------------

--
-- Table structure for table `userlog`
--

CREATE TABLE `userlog` (
  `logid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `log_time` varchar(255) NOT NULL,
  `action` enum('login','logout') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlog`
--

INSERT INTO `userlog` (`logid`, `userid`, `log_time`, `action`) VALUES
(1, 1, '2017-04-24 16:20:21', 'login'),
(2, 1, '2017-04-24 16:51:12', 'login'),
(3, 1, '2017-04-24 17:44:12', 'login'),
(4, 1, '2017-04-26 05:35:19', 'login'),
(5, 1, '2017-04-28 05:51:39', 'login'),
(6, 1, '2017-04-28 06:22:11', 'login'),
(7, 1, '2017-04-28 06:27:55', 'login'),
(8, 1, '2017-04-28 09:43:08', 'login'),
(9, 1, '2017-04-28 09:43:42', 'logout'),
(10, 1, '2017-04-28 09:44:07', 'login'),
(11, 1, '2017-04-28 09:48:33', 'login'),
(12, 1, '2017-04-28 09:51:59', 'logout'),
(13, 1, '2017-04-28 09:52:31', 'login'),
(14, 1, '2017-04-28 09:55:07', 'login'),
(15, 1, '2017-04-28 10:16:42', 'login'),
(16, 1, '2017-04-28 10:21:52', 'login'),
(17, 1, '2017-04-28 14:26:15', 'login'),
(18, 1, '2017-04-28 15:01:12', 'login'),
(19, 1, '2017-04-28 16:49:53', 'login'),
(20, 1, '2017-04-29 03:35:05', 'login'),
(21, 1, '2017-04-29 07:33:56', 'login'),
(22, 1, '2017-04-29 07:55:43', 'login'),
(23, 1, '2017-04-29 09:59:32', 'login'),
(24, 1, '2017-04-29 11:29:20', 'login'),
(25, 1, '2017-04-29 14:23:29', 'login'),
(26, 1, '2017-04-29 15:32:45', 'login'),
(27, 1, '2017-04-30 08:10:23', 'login'),
(28, 1, '2017-04-30 08:16:28', 'login'),
(29, 1, '2017-04-30 08:17:30', 'login'),
(30, 1, '2017-04-30 09:39:07', 'login'),
(31, 1, '2017-04-30 10:04:01', 'login'),
(32, 1, '2017-04-30 11:42:55', 'login'),
(33, 1, '2017-04-30 13:55:18', 'login'),
(34, 1, '2017-04-30 14:04:19', 'login'),
(35, 1, '2017-04-30 15:10:22', 'logout'),
(36, 1, '2017-04-30 15:22:16', 'login'),
(37, 1, '2017-05-01 01:00:36', 'login'),
(38, 1, '2017-05-01 01:36:38', 'login'),
(39, 1, '2017-05-01 02:52:12', 'login'),
(40, 1, '2017-05-01 03:48:03', 'login'),
(41, 1, '2017-05-01 04:04:41', 'login'),
(42, 1, '2017-05-01 04:11:08', 'login'),
(43, 1, '2017-05-01 04:20:37', 'login'),
(44, 1, '2017-05-01 05:01:17', 'login'),
(45, 1, '2017-05-01 05:04:13', 'login'),
(46, 1, '2017-05-01 08:38:32', 'login'),
(47, 1, '2017-05-01 10:14:39', 'login'),
(48, 1, '2017-05-01 12:43:37', 'login'),
(49, 1, '2017-05-01 14:58:39', 'login'),
(50, 1, '2017-05-01 14:59:29', 'logout'),
(51, 1, '2017-05-01 15:02:36', 'login'),
(52, 1, '2017-05-02 00:17:24', 'login'),
(53, 1, '2017-05-02 14:52:04', 'login'),
(54, 1, '2017-05-02 15:52:51', 'login'),
(55, 1, '2017-05-03 03:26:04', 'login'),
(56, 1, '2017-05-03 03:43:44', 'login'),
(57, 1, '2017-05-03 09:19:28', 'login'),
(58, 1, '2017-05-03 11:49:13', 'login'),
(59, 1, '2017-05-04 00:48:37', 'login'),
(60, 1, '2017-05-04 11:19:31', 'login'),
(61, 1, '2017-05-04 11:46:17', 'login'),
(62, 1, '2017-05-04 13:03:18', 'login'),
(63, 1, '2017-05-04 15:10:43', 'login'),
(64, 1, '2017-05-05 05:08:02', 'login'),
(65, 1, '2017-05-05 08:54:17', 'login'),
(66, 1, '2017-05-05 09:50:33', 'login'),
(67, 1, '2017-05-05 10:35:01', 'login'),
(68, 1, '2017-05-05 10:40:07', 'login'),
(69, 1, '2017-05-05 10:43:55', 'login'),
(70, 1, '2017-05-05 16:02:25', 'login'),
(71, 1, '2017-05-06 07:56:59', 'login'),
(72, 1, '2017-05-06 14:48:31', 'login'),
(73, 1, '2017-05-07 04:34:40', 'login'),
(74, 1, '2017-05-07 04:36:47', 'login'),
(75, 1, '2017-05-07 05:53:49', 'login'),
(76, 1, '2017-05-07 06:46:23', 'login'),
(77, 1, '2017-05-07 07:37:58', 'login'),
(78, 1, '2017-05-07 08:55:18', 'logout'),
(79, 1, '2017-05-07 08:56:16', 'login'),
(80, 1, '2017-05-07 11:43:07', 'login'),
(81, 1, '2017-05-07 13:29:49', 'login'),
(82, 1, '2017-05-08 00:38:51', 'login'),
(83, 1, '2017-05-08 10:36:43', 'login'),
(84, 1, '2017-05-08 15:24:37', 'login'),
(85, 1, '2017-05-08 15:44:32', 'login'),
(86, 1, '2017-05-09 06:42:57', 'login'),
(87, 1, '2017-05-09 09:51:03', 'login'),
(88, 1, '2017-05-09 12:07:30', 'login'),
(89, 1, '2017-05-10 02:58:06', 'login'),
(90, 1, '2017-05-10 04:54:48', 'login'),
(91, 1, '2017-05-10 06:49:05', 'login'),
(92, 1, '2017-05-10 07:29:36', 'login'),
(93, 1, '2017-05-10 11:17:45', 'login'),
(94, 1, '2017-05-11 05:05:16', 'login'),
(95, 1, '2017-05-12 04:26:20', 'login'),
(96, 1, '2017-05-12 10:56:48', 'login'),
(97, 1, '2017-05-13 12:18:29', 'login'),
(98, 1, '2017-05-14 13:00:47', 'login'),
(99, 1, '2017-05-15 04:12:31', 'login'),
(100, 1, '2017-05-15 04:42:56', 'login'),
(101, 1, '2017-05-15 10:23:59', 'logout'),
(102, 1, '2017-05-15 10:24:20', 'login'),
(103, 1, '2017-05-15 10:46:07', 'login'),
(104, 1, '2017-05-15 12:16:24', 'login'),
(105, 1, '2017-05-16 04:32:07', 'login'),
(106, 1, '2017-05-16 05:54:05', 'login'),
(107, 1, '2017-05-16 09:34:34', 'login'),
(108, 1, '2017-05-16 09:35:37', 'logout'),
(109, 1, '2017-05-16 14:16:30', 'login'),
(110, 1, '2017-05-17 01:49:05', 'login'),
(111, 1, '2017-05-17 04:48:29', 'login'),
(112, 1, '2017-05-17 05:04:30', 'login'),
(113, 1, '2017-05-17 11:02:26', 'login'),
(114, 1, '2017-05-17 11:28:28', 'login'),
(115, 1, '2017-05-18 02:29:41', 'login'),
(116, 1, '2017-05-18 04:37:09', 'login'),
(117, 1, '2017-05-18 05:31:53', 'login'),
(118, 1, '2017-05-18 07:30:17', 'login'),
(119, 1, '2017-05-18 10:10:27', 'login'),
(120, 1, '2017-05-18 11:28:50', 'login'),
(121, 1, '2017-05-18 11:29:16', 'logout'),
(122, 1, '2017-05-19 04:38:46', 'login'),
(123, 1, '2017-05-19 10:28:59', 'logout'),
(124, 1, '2017-05-19 10:30:27', 'login'),
(125, 1, '2017-05-20 02:31:54', 'login'),
(126, 1, '2017-05-20 04:37:36', 'login'),
(127, 1, '2017-05-20 08:08:52', 'login'),
(128, 1, '2017-05-21 04:31:42', 'login'),
(129, 1, '2017-05-21 07:18:05', 'login'),
(130, 1, '2017-05-21 11:41:15', 'login'),
(131, 1, '2017-05-21 15:35:51', 'login'),
(132, 1, '2017-05-21 16:17:13', 'login'),
(133, 1, '2017-05-22 04:19:36', 'login'),
(134, 1, '2017-05-22 06:53:30', 'logout'),
(135, 1, '2017-05-22 06:53:50', 'login'),
(136, 1, '2017-05-22 07:40:27', 'logout'),
(137, 1, '2017-05-22 07:40:31', 'login'),
(138, 1, '2017-05-22 10:51:26', 'login'),
(139, 1, '2017-05-22 14:58:13', 'login'),
(140, 1, '2017-05-23 04:32:59', 'login'),
(141, 1, '2017-05-24 02:27:37', 'login'),
(142, 1, '2017-05-24 04:36:06', 'login'),
(143, 1, '2017-05-24 13:01:38', 'login'),
(144, 1, '2017-05-25 01:49:33', 'login'),
(145, 1, '2017-05-25 05:35:45', 'login'),
(146, 1, '2017-05-26 05:09:02', 'login'),
(147, 1, '2017-05-26 10:15:40', 'login'),
(148, 1, '2017-05-26 10:38:27', 'login'),
(149, 1, '2017-05-27 01:38:57', 'login'),
(150, 1, '2017-05-28 04:27:09', 'login'),
(151, 1, '2017-05-29 05:25:03', 'login'),
(152, 1, '2017-05-29 05:40:36', 'login'),
(153, 1, '2017-05-29 09:38:15', 'login'),
(154, 1, '2017-05-30 06:05:28', 'login'),
(155, 1, '2017-05-30 08:39:01', 'login'),
(156, 1, '2017-05-30 08:48:15', 'login'),
(157, 1, '2017-05-31 10:10:23', 'login'),
(158, 1, '2017-05-31 14:50:31', 'login'),
(159, 1, '2017-06-01 04:54:46', 'login'),
(160, 1, '2017-06-01 05:27:44', 'login'),
(161, 1, '2017-06-01 05:47:11', 'login'),
(162, 1, '2017-06-01 05:49:21', 'login'),
(163, 1, '2017-06-01 06:21:32', 'login'),
(164, 1, '2017-06-01 10:05:22', 'login'),
(165, 1, '2017-06-02 04:51:27', 'login'),
(166, 1, '2017-06-02 07:07:01', 'login'),
(167, 1, '2017-06-03 06:19:31', 'login'),
(168, 1, '2017-06-04 04:39:17', 'login'),
(169, 1, '2017-06-07 07:34:36', 'login'),
(170, 1, '2017-06-19 09:04:43', 'login'),
(171, 1, '2017-06-22 07:20:12', 'login'),
(172, 1, '2017-06-25 08:04:05', 'login'),
(173, 1, '2017-07-03 08:06:33', 'login'),
(174, 1, '2017-07-06 17:08:47', 'login'),
(175, 1, '2017-07-16 07:12:32', 'login'),
(176, 1, '2017-08-10 04:02:57', 'login'),
(177, 1, '2017-08-10 04:14:51', 'login'),
(178, 1, '2017-08-10 04:33:45', 'logout'),
(179, 1, '2017-08-10 04:33:51', 'login'),
(180, 1, '2017-08-10 04:54:01', 'logout'),
(181, 1, '2017-08-10 05:26:18', 'logout'),
(182, 2, '2017-08-10 05:27:19', 'login'),
(183, 2, '2017-08-22 16:01:30', 'login'),
(184, 1, '2017-08-23 03:27:58', 'login'),
(185, 1, '2017-08-23 10:25:10', 'login'),
(186, 1, '2017-09-16 17:07:10', 'login'),
(187, 1, '2017-10-01 05:37:40', 'login'),
(188, 1, '2017-10-01 11:30:26', 'login');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `user_initials` varchar(3) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `user_group` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `added_date` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `employee_id`, `user_initials`, `first_name`, `last_name`, `email`, `phone`, `user_group`, `user_name`, `password`, `added_date`, `added_by`, `modified_date`, `modified_by`, `status`) VALUES
(1, 0, NULL, 'Shruti', '', '', '', 0, 'admin', 'e6e061838856bf47e1de730719fb2609', '2017-01-22', 0, NULL, NULL, 1),
(2, 0, 'AS', 'Super', 'Admin', '', '  97714427777', 1, 'centraladmin', 'e6e061838856bf47e1de730719fb2609', '2013-12-05', 1, '2014-12-30', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_actions`
--

CREATE TABLE `user_actions` (
  `user_action_id` int(11) NOT NULL,
  `user_action_name` varchar(15) NOT NULL,
  `user_action_code` varchar(15) NOT NULL,
  `addded_by` int(11) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_actions`
--

INSERT INTO `user_actions` (`user_action_id`, `user_action_name`, `user_action_code`, `addded_by`, `added_date`, `modified_by`, `modified_date`) VALUES
(1, 'ADD', 'ADD', 1, '2014-02-20 00:00:00', NULL, NULL),
(2, 'MODIFY', 'MODIFY', 1, '2014-02-20 00:00:00', NULL, NULL),
(3, 'DELETE', 'DELETE', 1, '2014-02-20 00:00:00', NULL, NULL),
(4, 'VIEW', 'VIEW', 1, '2014-02-20 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`menuid`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`groupid`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permid`);

--
-- Indexes for table `permissions_per_group`
--
ALTER TABLE `permissions_per_group`
  ADD PRIMARY KEY (`permission_per_group_id`);

--
-- Indexes for table `permissions_per_user`
--
ALTER TABLE `permissions_per_user`
  ADD PRIMARY KEY (`permission_per_user_id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userkeys`
--
ALTER TABLE `userkeys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userlog`
--
ALTER TABLE `userlog`
  ADD PRIMARY KEY (`logid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `user_actions`
--
ALTER TABLE `user_actions`
  ADD PRIMARY KEY (`user_action_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `menuid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `groupid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions_per_group`
--
ALTER TABLE `permissions_per_group`
  MODIFY `permission_per_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `permissions_per_user`
--
ALTER TABLE `permissions_per_user`
  MODIFY `permission_per_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `userkeys`
--
ALTER TABLE `userkeys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `userlog`
--
ALTER TABLE `userlog`
  MODIFY `logid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_actions`
--
ALTER TABLE `user_actions`
  MODIFY `user_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
