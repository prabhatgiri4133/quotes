<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mylibrary{

    function __construct(){
        $this->CI=& get_instance();
        $this->CI->load->helper('form');
        $this->CI->load->helper('url');
    }

    function getsublinks($parent_id){
        $links = '';
        $user_id = $this->CI->session->userdata('userid');
        $gid = $this->CI->session->userdata('usergroup');
        $this->CI->db->where('menuid',$parent_id);
        $qmenu = $this->CI->db->get('admin_menu',1);
        $rmenu = $qmenu->row();
        if($gid!=1){
            $sql = "SELECT * FROM admin_menu m WHERE fn_CheckMenuPermission(m.menuid,". $user_id .") = 1 AND  m.parent_id='".$parent_id."' AND m.group_label='' AND m.status=1 ORDER BY m.position";
            $query = $this->CI->db->query($sql);
            if($query->num_rows()>0){
                $links .= '<div class="panel panel-default sidebar">
							  <div class="panel-heading">'.$rmenu->menu_name.'</div>
							  <ul class="nav nav-stacked">';
                foreach($query->result() as $row):
                    $links .= '<li>'.anchor($row->menu_link,$row->menu_name).'</li>';
                endforeach;
                $links .= '</ul>
							</div>';
            }
            $query->free_result();
            $qsub = "SELECT DISTINCT(group_label) AS group_label FROM admin_menu m WHERE m.parent_id='".$parent_id."' AND m.group_label!='' AND fn_CheckMenuPermission(m.menuid,". $user_id .") = 1 AND m.status=1 ORDER BY m.position";
            $qgroup = $this->CI->db->query($qsub);
            if($qgroup->num_rows()>0){
                foreach($qgroup->result() as $rgroup):
                    $links .= '<div class="panel panel-default sidebar">
								   <div class="panel-heading">'.$rgroup->group_label.'</div>
								   <ul class="nav nav-stacked">';
                    $sqlm = "SELECT * FROM admin_menu m WHERE m.parent_id='".$parent_id."' AND m.group_label='".$rgroup->group_label."' AND fn_CheckMenuPermission(m.menuid,". $user_id .") = 1 AND m.status=1 ORDER BY m.position";
                    $mquery = $this->CI->db->query($sqlm);
                    if($mquery->num_rows()>0){
                        foreach($mquery->result() as $rmenu):
                            $links .= '<li>'.anchor($rmenu->menu_link,$rmenu->menu_name).'</li>';
                        endforeach;
                    }
                    $mquery->free_result();
                    $links .= '</ul>
								</div>';
                endforeach;
            }


            $qgroup->free_result();
        }else{
            $sql = "SELECT * FROM admin_menu m WHERE  m.parent_id='".$parent_id."' AND m.group_label='' AND m.status=1 ORDER BY m.position";
            $query = $this->CI->db->query($sql);
            if($query->num_rows()>0){
                $links .= '<div class="panel panel-default sidebar">
							  <div class="panel-heading">'.$rmenu->menu_name.'</div>
							  <ul class="nav nav-stacked">';
                foreach($query->result() as $row):
                    $links .= '<li>'.anchor($row->menu_link,$row->menu_name).'</li>';
                endforeach;
                $links .= '</ul>
							</div>';
            }
            $query->free_result();

            $qsub = "SELECT DISTINCT(group_label) as group_label FROM admin_menu m WHERE m.parent_id='".$parent_id."' AND m.group_label!='' AND m.status=1 ORDER BY m.position";
            $qgroup = $this->CI->db->query($qsub);
            if($qgroup->num_rows()>0){
                foreach($qgroup->result() as $rgroup):
                    $links .= '<div class="panel panel-default sidebar">
								   <div class="panel-heading">'.$rgroup->group_label.'</div>
								   <ul class="nav nav-stacked">';
                    $sqlm = "SELECT * FROM admin_menu m WHERE m.parent_id='".$parent_id."' AND m.group_label='".$rgroup->group_label."' AND m.status=1 ORDER BY m.position";
                    $mquery = $this->CI->db->query($sqlm);
                    if($mquery->num_rows()>0){
                        foreach($mquery->result() as $rmenu):
                            $links .= '<li>'.anchor($rmenu->menu_link,$rmenu->menu_name).'</li>';
                        endforeach;
                    }
                    $mquery->free_result();
                    $links .= '</ul>
								</div>';
                endforeach;
            }
            $qgroup->free_result();
            $qmenu->free_result();

        }
        return $links;
    }

    function getPermissions($group_id){
        $permission = array();
        $this->CI->db->where('group_id',$group_id);
        $query = $this->CI->db->get('permissions_per_group');
        if($query->num_rows()>0){
            foreach($query->result() as $row):
                $permission[] = $row->module_id;
            endforeach;
        }
        return $permission;
    }

    function getMenuInformation(){
        if($this->CI->uri->segment(2)!=''){
            $link = $this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2).'/';
        }else{
            $link = $this->CI->uri->segment(1).'/';
        }
        $this->CI->db->where('menu_link',$link);
        $this->CI->db->where('parent_id !=','0');
        $query = $this->CI->db->get('admin_menu');
        if($query->num_rows()>0){
            $row = $query->row();
            return $row->description;
        }else{
            return '';
        }
    }

    function getgeneralsetting($optname){
        $this->CI->db->select('option_value');
        $this->CI->db->where('option_name', $optname);
        $query = $this->CI->db->get('general_settings');
        if($query->num_rows()>0){
            $row=$query->row();
            $optionname=$row->option_value;
        }else{
            $optionname='';
        }
        return $optionname;
    }

    function checkperm($module_code, $user_action_code){
        $login_id = $this->CI->session->userdata('userid');
        $sql = "SELECT fn_CheckPermissionByLoginId('". $module_code . "','". $user_action_code . "','". $login_id ."') AS permission";
        $query = $this->CI->db->query($sql);
        $result = $query->row();
        if(!$result->permission){
            $this->CI->session->set_flashdata("error", "Unauthorized Access!");
            redirect('dashboard','location');
        } else {
            return true;
        }

    }

    function generateProjectReferenceCode()
    {
        $refcode = 'PRJ-'.date('ymd-His');
        return $refcode;
    }

    function generateIssueCode()
    {
        $code = 'ISS-'.date('ymd-His');
        return $code;
    }

    function generateEmployeeCode()
    {
        $code = 'EMP-'.date('ymd-His');
        return $code;
    }

    function getContactList($type){
        $this->CI->db->like('contact_type',$type, 'both');
        $this->CI->db->where('status',1);
        return $this->CI->db->get('addressbook');
    }

    function getStatusList($order='')
    {
        ($order != '')?$this->CI->db->where('status_order >=',$order):'';
        $this->CI->db->order_by('status_order');
        return $this->CI->db->get('project_status');
    }
}