<?php
    class NotificationModel extends CI_Model{
        function __construct(){
            parent::__construct();  
        }

        /**
         * Author: Prabhat Giri
         * Last Modified: 2/7/2017
         * [Share sends firebase notification]
         * @param single value or array  $receiverToken     firebase id's who will receive notification
         * @param array $notificationdata                   notification text and other data
         * @param array $databody                           notification body data that will be used by the app
         */
        public function index($receiverToken,$notificationdata,$databody)
        {

            $url        = 'https://gcm-http.googleapis.com/gcm/send'; //default url to send firebase notification. do not change unless changed by google

            $server_key = 'AIzaSyDQgMaT_VXjJvTk4EFfonR-Ui3CsY2wbZw'; // enter your legacy cloud messaging API key here
            
            $target     = $receiverToken; 
            $msg        = $notificationdata;
            $data       = $databody;

            $fields                 = array();
            $fields['notification'] = $msg;
            $fields['data']         = $data;
            if(is_array($target)){
                $fields['registration_ids'] = $target;
            }else{
                $fields['to']               = json_encode($target);
            }

            //header with content_type api key
            $headers = array(
                'Content-Type:application/json',

                'Authorization:key='.$server_key
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                return ('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
            echo "<pre>";var_dump($result);
            die();
            return 1;
        }
    }
?>