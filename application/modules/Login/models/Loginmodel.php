<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/1/16
 * Time: 2:53 PM
 */
class Loginmodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'users';
        $this->logtable = 'userlog';
    }

    public function getLoggedInUserDetails($username, $password){
		$this->db->group_start();
		$this->db->where('user_name', $username);
		$this->db->or_where('email', $username);
		$this->db->group_end();
        $this->db->where('password', $password);
        $this->db->where('Status', 1);
        return $this->db->get($this->table);
    }

    public function setUserLog($data){
        $this->db->insert($this->logtable, $data);
    }
	
	public function getUserInfo($userid){
		$this->db->where('userid', $userid);
		return $this->db->get($this->table);
	}
	
	public function updateUserPassword($id, $data){
		$this->db->where('userid', $id);
		$this->db->update($this->table, $data);
	}

	public function getFirstLoginInfo($id){
        $this->db->where('userid', $id);
        $this->db->where('first_login',1);
        return $this->db->get($this->table);
    }
}