<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/1/16
 * Time: 2:13 PM
 */
class Login extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Loginmodel');
    }

     public function _remap($method)
        {
            if(method_exists($this,$method))
            {
                call_user_func(array($this, $method));
                return false;
            }
            else
            {
                call_user_func(array($this, 'Index'));
                return false;
            }
        }


    public function Index(){
        if($this->authlibrary->IsLoggedIn()) {
            if($this->session->userdata('redirect_url)'))
            {
                redirect($this->session->userdata('redirect_url'));
            }
            else
            {
                redirect('Quote');
            }
        }else{
            if ($this->input->post('Login')) {
                $username = $this->input->post('Username');
                $password = $this->input->post('Password');

                // Get the record of the user according to the login details provided
                $userdetails = $this->Loginmodel->getLoggedInUserDetails($username, md5($password));
                if ($userdetails->num_rows() > 0) {
					// Check if the user if first login, if its first login, then redirect them to a seperate page to update new password
					// if($userdetails->row()->first_login == 1){
					// 	redirect('Login/SetPassword/'.$userdetails->row()->userid);
					// }
                    // Set all the user details in the session
                    $userdata = array(
                        'PRJ_USER_ID' => $userdetails->row()->userid,
                        'PRJ_USER_GROUP' => $userdetails->row()->user_group,
                        'PRJ_USER_NAME' => $userdetails->row()->user_name,
                        'PRJ_EMPLOYEE_ID' => $userdetails->row()->employee_id,
                        'PRJ_USER_NAME_FIRST' =>  $userdetails->row()->first_name,
                        'PRJ_USER_NAME_SECOND' => $userdetails->row()->last_name,
                        'PRJ_USER_EMAIL' => $userdetails->row()->email,
                    );
                    $this->session->set_userdata($userdata);

                    // Update the log table
                    $data['userlog']['userid'] = $userdetails->row()->userid;
                    $data['userlog']['log_time'] = date('Y-m-d H:i:s');
                    $data['userlog']['action'] = 'login';

                    $this->Loginmodel->setUserLog($data['userlog']);

                    // Redirect to homepage
                    redirect('Quote');
                } else {
                    $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN', 'Incorrect Login Credentials.');
                    redirect('Login');
                }
            }
            // $data['pagetitle'] = 'Home';
            // $data['page'] = 'homeview';
            $this->load->view('login');
        }
    }    
}