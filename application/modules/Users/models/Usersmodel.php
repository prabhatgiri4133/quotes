<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/2/16
 * Time: 10:03 PM
 */
class Usersmodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listUser(){
        $this->db->select('u.status, user_name, group_name, user_group, first_name, last_name, email,phone, u.userid, u.added_date, l.log_time');
        $this->db->from('users u');
        $this->db->join('group g','g.groupid=u.user_group');
        $this->db->join('userlog l','u.userid=l.userid','left');
        $this->db->where('u.status !=','2');
        $this->db->where('u.userid!=','0');
        $this->db->group_start();
            $this->db->where('l.action','login');
            $this->db->order_by('l.log_time','desc');    
            $this->db->limit(1);
        $this->db->group_end();
        $this->db->order_by('first_name','asc');
        return $this->db->get();
    }
    public function listUserByID($id){
        $this->db->select('u.status, user_name, group_name, user_group, first_name, last_name, email,phone, userid, u.added_date');
        $this->db->from('users u');
        $this->db->join('group g','g.groupid=u.user_group');
        $this->db->where('userid',$id);
        $this->db->where('u.status !=','2');
        $this->db->where('userid!=','0');
        $this->db->order_by('first_name','asc');
        return $this->db->get();
    }
    function listgroup($limit = null){
        $this->db->where('status',0);
        $this->db->where('groupid!=','1');
        (!$limit == null)?$this->db->limit($limit['start'],$limit['end']):"";
        return $this->db->get('group');
    }
    function insertuser($data)
    {
        $this->db->insert('users', $data);
    }
    function updateuser($userid,$data)
    {
        $this->db->where('userid',$userid);
        $this->db->update('users',$data);
    }
    function getUserData($id)
    {
        $this->db->where('userid',$id);
        $query = $this->db->get('users');
        if($query->num_rows()>0)
        {
            return $query->row();
        }
        else
        {
            return '';
        }
    }
    function addGroup($data){
            $this->db->insert('group',$data);   
    }
    function getGroup($id){
        $this->db->where('groupid',$id);
        $query = $this->db->get('group');
        if($query->num_rows()>0){
            return $query->row();
        }
        else{
            return '';
        }
    }
    function editGroup($data,$id){
        $this->db->where('groupid',$id);
        $this->db->update('group',$data);
    }
    function listmodule($parent_id = 0,$limit=null){
        $this->db->select('*');
        $this->db->from('admin_menu');
        $this->db->where('parent_id',$parent_id);
        if($this->session->userdata('usergroup')!=1){
            $this->db->where('status','1');
        }
        (!$limit == null)?$this->db->limit($limit['start'],$limit['end']):"";
        $res = $this->db->get();
        return $res;    
    }
    function getgroupname($group_id){
        $this->db->select('group_name');
        $this->db->where('groupid',$group_id);
        $query = $this->db->from('group');
        $result = $this->db->get();
        $group = $result->row();
        return $group->group_name;
    }
    function listuseraction($limit=null){
        $this->db->select('*');
        $this->db->from('user_actions');
        (!$limit == null)?$this->db->limit($limit['start'],$limit['end']):"";
        $res = $this->db->get();
        return $res;
    }
    function checkgroup_permision($module_id, $user_action_id, $group_id){
        $query = "SELECT 
                    fn_CheckGroupPermission(". $module_id .",". $user_action_id .",". $group_id .")
                  AS permission
                 ";
        $result = $this->db->query($query);
        $permission = $result->row();
        return $permission->permission;
    }
    function updategroup_permision($permissions, $group_id, $login_id){
        $this->db->trans_start();
        $permission_set = '';
        if(count($permissions)>0){
            $permission_set = implode(",", $permissions);               
        }
        $parameters = array($permission_set, $group_id,$login_id);
        $qry_res = $this->db->query('CALL sp_InsertGroupPermission(?,?,?)', $parameters);
        $this->db->trans_complete();            
    }
    function getgroupid($user_id){
        $this->db->select('user_group');
        $this->db->where('userid',$user_id);
        $query = $this->db->from('users');
        $result = $this->db->get();
        $group = $result->row();
        return $group->user_group;
    }   
    function checkuser_perm($module_id, $user_action_id, $user_id){
        $query = "SELECT 
                    fn_CheckUserPermission(". $module_id .",". $user_action_id .",". $user_id .")
                  AS permission
                 ";
        $result = $this->db->query($query);
        $permission = $result->row();
        return $permission->permission;
    }
    function updateuser_perm($permissions, $user_id, $login_id){
        $this->db->trans_start();
        $permission_set = '';
        if(count($permissions)>0){
            $permission_set = implode(",", $permissions);               
        }
        $parameters = array($permission_set, $user_id, $login_id);
        $qry_res = $this->db->query('CALL sp_InsertUserPermission(?,?,?)', $parameters);
        $this->db->trans_complete();
    }
    function listEmployee()
    {
        $this->db->select('employee_id as ID, fullname as Title');
        return $this->db->get('employee');
    }
    function GetEmployeeData($id)
    {
        $this->db->select('fullname, email, mobile');
        $this->db->where('employee_id',$id);
        $query = $this->db->get('employee');
        if($query->num_rows()>0)
        {
            return $query->row();
        }
        else
        {
            return array();
        }
    }
}