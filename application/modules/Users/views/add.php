<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <?php echo $breadcrumb;?>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> <?php echo $pagetitle;?></h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <?php echo form_open('Users/Add', array('name'=>'AddUser', 'id'=>'AddUser', 'method'=>'post', 'class'=>'form-horizontal'));?>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="actions">
                            <a href="javascript:;" id="save_button" class="btn save_button green"><i class="fa fa-save"></i> Save</a>
                            <a href="javascript:;" id="cancel_button" class="btn red"><i class="fa fa-remove"></i> Cancel</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="UserRole" class="control-label col-md-3">Role</label>
                                    <div class="col-md-9">
                                        <?php echo form_dropdown('role', $group,'',array('id'=>'UserRole', 'class'=>'bs-select form-control', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="FirstName" class="control-label col-md-3">First Name</label>
                                    <div class="col-md-9">
                                        <?php echo form_input(array('name'=>'fname', 'id'=>'FirstName', 'class'=>'form-control', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="LastName" class="control-label col-md-3">Last Name</label>
                                    <div class="col-md-9">
                                        <?php echo form_input(array('name'=>'lname', 'id'=>'LastName', 'class'=>'form-control'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Email" class="control-label col-md-3">Email</label>
                                    <div class="col-md-9">
                                        <?php echo form_input(array('name'=>'email', 'type'=>'email', 'id'=>'Email', 'class'=>'form-control', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Phone" class="control-label col-md-3">Phone</label>
                                    <div class="col-md-9">
                                        <?php echo form_input(array('name'=>'phone', 'id'=>'Phone', 'class'=>'form-control'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Username" class="control-label col-md-3">Username</label>
                                    <div class="col-md-9">
                                        <?php echo form_input(array('name'=>'username', 'id'=>'Username', 'class'=>'form-control', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Password" class="control-label col-md-3">Password</label>
                                    <div class="col-md-9">
                                        <?php echo form_password(array('name'=>'password', 'id'=>'Password', 'class'=>'form-control password', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="CPassword" class="control-label col-md-3">Confirm Password</label>
                                    <div class="col-md-9">
                                        <?php echo form_password(array('name'=>'cpassword', 'id'=>'CPassword', 'class'=>'form-control cpassword', 'required'=>'required'));?>
                                        <?php echo form_input(array('type'=>'submit','name'=>'Submit', 'id'=>'Submit','value'=>'Submit', 'class'=>'form-control hidden', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->