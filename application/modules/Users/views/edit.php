<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <?php echo $breadcrumb;?>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> <?php echo $pagetitle;?></h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <?php echo form_open('Users/EditUser', array('name'=>'EditUser', 'id'=>'EditUser', 'method'=>'post', 'class'=>'form-horizontal'));?>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="actions">
                            <a href="javascript:;" id="save_button" class="btn green save_button"><i class="fa fa-save"></i> Save</a>
                            <a href="javascript:;" id="cancel_button" class="btn red"><i class="fa fa-remove"></i> Cancel</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="UserRole" class="control-label col-md-3">Role</label>
                                    <div class="col-md-9">
                                        <?php echo form_dropdown('role', $group,$query->user_group,array('id'=>'UserRole', 'class'=>'form-control', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="FirstName" class="control-label col-md-3">First Name</label>
                                    <div class="col-md-9">
                                    <?php $name = (isset($query->first_name) && $query->first_name!='')?$query->first_name:''?>
                                        <?php echo form_input(array('name'=>'fname', 'id'=>'FirstName', 'class'=>'form-control', 'value'=>$name));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="LastName" class="control-label col-md-3">Last Name</label>
                                    <div class="col-md-9">
                                    <?php $lname = (isset($query->last_name) && $query->last_name!='')?$query->last_name:''?>
                                        <?php echo form_input(array('name'=>'lname', 'id'=>'LastName', 'class'=>'form-control','value'=>$lname));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Email" class="control-label col-md-3">Email</label>
                                    <div class="col-md-9">
                                    <?php $email = (isset($query->email) && $query->email!='')?$query->email:''?>
                                        <?php echo form_input(array('name'=>'email', 'type'=>'email', 'id'=>'Email', 'class'=>'form-control', 'required'=>'required','value'=>$email));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Phone" class="control-label col-md-3">Phone</label>
                                    <div class="col-md-9">
                                    <?php $phone = (isset($query->phone) && $query->phone!='')?$query->phone:''?>
                                        <?php echo form_input(array('name'=>'phone', 'id'=>'Phone', 'class'=>'form-control','value'=>$phone));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Username" class="control-label col-md-3">Username</label>
                                    <div class="col-md-9">
                                    <?php $username = (isset($query->user_name) && $query->user_name!='')?$query->user_name:''?>
                                        <?php echo form_input(array('name'=>'username', 'id'=>'Username', 'class'=>'form-control', 'required'=>'required','value'=>$username));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="changepassword" class="control-label col-md-3">Change Password?</label>
                                    <div class="col-md-9">
                                    <?php echo form_input(array('type'=>'radio','name'=>'changepassword', 'id'=>'changepassword', 'class'=>'changepassword', 'required'=>'required','value'=>'Yes'))?>Yes
                                    <?php echo form_input(array('type'=>'radio','name'=>'changepassword', 'id'=>'changepassword', 'class'=>'changepassword', 'required'=>'required','value'=>'No','checked'=>'checked'))?>No
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row changepasswordform hidden">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Password" class="control-label col-md-3">Password</label>
                                    <div class="col-md-9">
                                        <?php echo form_password(array('name'=>'password', 'id'=>'Password', 'class'=>'form-control password'));?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="CPassword" class="control-label col-md-3">Confirm Password</label>
                                    <div class="col-md-9">
                                        <?php echo form_password(array('name'=>'cpassword', 'id'=>'CPassword', 'class'=>'form-control cpassword'));?>
                                        <?php echo form_input(array('type'=>'submit','name'=>'Submit', 'id'=>'Submit','value'=>'Submit', 'class'=>'form-control hidden', 'required'=>'required'));?>
                                        <?php echo form_input(array('type'=>'hidden','name'=>'UserID', 'id'=>'UserID','value'=>$this->uri->segment(3), 'class'=>'form-control', 'required'=>'required'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->



<section id="content">
    <div class="container">
        <div class="card">
            <div class="action-header palette-Teal-400 bg clearfix">
                <div class="ah-label hidden-xs palette-White text"><?php echo $title?></div>

                <div class="ah-search">
                    <input type="text" placeholder="Start typing..." class="ahs-input">

                    <i class="ah-search-close zmdi zmdi-long-arrow-left" data-ma-action="ah-search-close"></i>
                </div>

                <ul class="ah-actions actions a-alt">
                    <li>
                        <a href="#" class="ah-search-trigger" data-ma-action="ah-search-open">
                            <i class="zmdi zmdi-search"></i>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i class="zmdi zmdi-time"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-sort"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#">Last Modified</a>
                            </li>
                            <li>
                                <a href="#">Last Edited</a>
                            </li>
                            <li>
                                <a href="#">Name</a>
                            </li>
                            <li>
                                <a href="#">Date</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="zmdi zmdi-info"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#">Refresh</a>
                            </li>
                            <li>
                                <a href="#">Listview Settings</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="list-group lg-alt lg-even-black">
                <div class="list-group-item media">
                    <div class="media-body">
                       <div class="row">
                            <div class="col-md-12">
                                <?php echo form_open('Users/EditUser', array('name'=>'EditUser', 'id'=>'EditUser', 'method'=>'post', 'class'=>'form-horizontal'));?>
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="actions">
                                            <a href="javascript:;" id="save_button" class="btn green save_button"><i class="fa fa-save"></i> Save</a>
                                            <a href="javascript:;" id="cancel_button" class="btn red"><i class="fa fa-remove"></i> Cancel</a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="UserRole" class="control-label col-md-3">Role</label>
                                                    <div class="col-md-9">
                                                        <?php echo form_dropdown('role', $group,$query->user_group,array('id'=>'UserRole', 'class'=>'form-control', 'required'=>'required'));?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="FirstName" class="control-label col-md-3">First Name</label>
                                                    <div class="col-md-9">
                                                    <?php $name = (isset($query->first_name) && $query->first_name!='')?$query->first_name:''?>
                                                        <?php echo form_input(array('name'=>'fname', 'id'=>'FirstName', 'class'=>'form-control', 'value'=>$name));?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="LastName" class="control-label col-md-3">Last Name</label>
                                                    <div class="col-md-9">
                                                    <?php $lname = (isset($query->last_name) && $query->last_name!='')?$query->last_name:''?>
                                                        <?php echo form_input(array('name'=>'lname', 'id'=>'LastName', 'class'=>'form-control','value'=>$lname));?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Email" class="control-label col-md-3">Email</label>
                                                    <div class="col-md-9">
                                                    <?php $email = (isset($query->email) && $query->email!='')?$query->email:''?>
                                                        <?php echo form_input(array('name'=>'email', 'type'=>'email', 'id'=>'Email', 'class'=>'form-control', 'required'=>'required','value'=>$email));?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Phone" class="control-label col-md-3">Phone</label>
                                                    <div class="col-md-9">
                                                    <?php $phone = (isset($query->phone) && $query->phone!='')?$query->phone:''?>
                                                        <?php echo form_input(array('name'=>'phone', 'id'=>'Phone', 'class'=>'form-control','value'=>$phone));?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Username" class="control-label col-md-3">Username</label>
                                                    <div class="col-md-9">
                                                    <?php $username = (isset($query->user_name) && $query->user_name!='')?$query->user_name:''?>
                                                        <?php echo form_input(array('name'=>'username', 'id'=>'Username', 'class'=>'form-control', 'required'=>'required','value'=>$username));?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="changepassword" class="control-label col-md-3">Change Password?</label>
                                                    <div class="col-md-9">
                                                    <?php echo form_input(array('type'=>'radio','name'=>'changepassword', 'id'=>'changepassword', 'class'=>'changepassword', 'required'=>'required','value'=>'Yes'))?>Yes
                                                    <?php echo form_input(array('type'=>'radio','name'=>'changepassword', 'id'=>'changepassword', 'class'=>'changepassword', 'required'=>'required','value'=>'No','checked'=>'checked'))?>No
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row changepasswordform hidden">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Password" class="control-label col-md-3">Password</label>
                                                    <div class="col-md-9">
                                                        <?php echo form_password(array('name'=>'password', 'id'=>'Password', 'class'=>'form-control password'));?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="CPassword" class="control-label col-md-3">Confirm Password</label>
                                                    <div class="col-md-9">
                                                        <?php echo form_password(array('name'=>'cpassword', 'id'=>'CPassword', 'class'=>'form-control cpassword'));?>
                                                        <?php echo form_input(array('type'=>'submit','name'=>'Submit', 'id'=>'Submit','value'=>'Submit', 'class'=>'form-control hidden', 'required'=>'required'));?>
                                                        <?php echo form_input(array('type'=>'hidden','name'=>'UserID', 'id'=>'UserID','value'=>$this->uri->segment(3), 'class'=>'form-control', 'required'=>'required'));?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>