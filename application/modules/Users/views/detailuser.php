<!-- BEGIN CONTENT -->



<section id="content">
    <div class="container">
        <div class="card">
            <div class="action-header palette-Teal-400 bg clearfix">
                <div class="ah-label hidden-xs palette-White text"><?php echo $title?></div>
            </div>

            <div class="list-group lg-alt lg-even-black">
                <div class="list-group-item media">
                    <div class="media-body">
                      <div class="col-md-12">
                            <?php echo form_open('Users/', array('name'=>'', 'id'=>'', 'method'=>'post', 'class'=>'form-horizontal'));?>
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="actions">
                                        <a href="<?php echo base_url();?>Users/EditUser/<?php echo $user->userid;?>" class="btn btn-primary waves-effect"><i class="fa fa-edit"></i> Edit</a>
                                        <a href="<?php echo base_url();?>Users/EditUserPerm/<?php echo $user->userid;?>" class="btn btn-primary waves-effect"><i class="fa fa-edit"></i> Change Permission</a>
                                        <a href="<?php echo base_url();?>Users/ListAll" class="btn btn-danger waves-effect"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="UserRole" class="control-label col-md-3 bold">Role</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo $user->user_group;?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="FirstName" class="control-label col-md-3 bold">First Name</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo (isset($user->first_name) && $user->first_name!='')?$user->first_name:''?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="LastName" class="control-label col-md-3 bold">Last Name</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo (isset($user->last_name) && $user->last_name!='')?$user->last_name:''?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="Email" class="control-label col-md-3 bold">Email</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo (isset($user->email) && $user->email!='')?$user->email:''?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="Phone" class="control-label col-md-3 bold">Phone</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo (isset($user->phone) && $user->phone!='')?$user->phone:''?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="Username" class="control-label col-md-3 bold">Username</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo (isset($user->user_name) && $user->user_name!='')?$user->user_name:''?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CONTENT -->