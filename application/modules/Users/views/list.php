<section id="content">
    <div class="container">
        <div class="card">
            <div class="action-header palette-Teal-400 bg clearfix">
                <div class="ah-label hidden-xs palette-White text"><?php echo $title?></div>

                <div class="ah-search">
                    <input type="text" placeholder="Start typing..." class="ahs-input">

                    <i class="ah-search-close zmdi zmdi-long-arrow-left" data-ma-action="ah-search-close"></i>
                </div>

                <ul class="ah-actions actions a-alt">
                    <li>
                        <a href="#" class="ah-search-trigger" data-ma-action="ah-search-open">
                            <i class="zmdi zmdi-search"></i>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i class="zmdi zmdi-time"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-sort"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#">Last Modified</a>
                            </li>
                            <li>
                                <a href="#">Last Edited</a>
                            </li>
                            <li>
                                <a href="#">Name</a>
                            </li>
                            <li>
                                <a href="#">Date</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="zmdi zmdi-info"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#">Refresh</a>
                            </li>
                            <li>
                                <a href="#">Listview Settings</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="list-group lg-alt lg-even-black">
                <div class="list-group-item media">
                    <div class="media-body">
                       <div class="table-responsive">
                            <table id="data-table-basic" data-toggle="bootgrid" class="table table-vmiddle table-condensed table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th data-column-id="name"> Name </th>
                                        <th data-column-id="username"> Username </th>
                                        <th data-column-id="group" data-order="desc"> Group </th>
                                        <th data-column-id="created"> Created On </th>
                                        <th data-column-id="last-login"> Last Logged In </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($users->num_rows()>0){
                                        foreach($users->result() as $user){
                                            ?>
                                            <tr>
                                                <td><?php echo anchor('Users/Detail/'.$user->userid,$user->first_name.' '.$user->last_name);?></td>
                                                <td><?php echo $user->user_name;?></td>
                                                <td><?php echo $user->group_name;?></td>
                                                <td><?php echo $user->added_date;?></td>
                                                <td><?php echo $user->log_time?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                            </table>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
</section>