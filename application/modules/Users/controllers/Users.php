<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/2/16
 * Time: 9:57 PM
 */
class Users extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usersmodel');
        $this->module_code = 'MANAGE-USER';
    }

    public function Index()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "VIEW")){
            redirect('Users/ListAll','location');
        }else{
            $this->session->set_flashdata('error','Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login','location');
        }
    }

    public function ListAll()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"VIEW")){
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Users' => 'Users/ListAll',
                'List All'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['users'] = $this->Usersmodel->listUser();
            $data['title'] 	= 'Manage Users';
            $data['pagetitle'] = 'Manage Users';
            $data['page'] = 'list';
            $data['script'] = 'listscript';
            $this->load->vars($data);
            $this->load->view('main');
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }
    public function Detail()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"VIEW")){
            $id = $this->uri->segment(3);
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Users' => 'Users/ListAll',
                'Detail'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['user'] = $this->Usersmodel->listUserByID($id)->row();
            $data['title']  = 'Manage Users';
            $data['pagetitle'] = 'Manage Users';
            $data['page'] = 'detailuser';
            $data['script'] = 'listscript';
            $this->load->vars($data);
            $this->load->view('main');
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }

    public function Add()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "ADD")) {
            if($this->input->post('Submit'))
            {
                $value['details']['first_name'] 	= $this->input->post('fname');
                $value['details']['last_name'] 		= $this->input->post('lname');
                $value['details']['email'] 			= $this->input->post('email');
                $value['details']['phone'] 			= $this->input->post('phone');
                $value['details']['user_group'] 	= $this->input->post('role');
                $value['details']['user_name'] 		= $this->input->post('username');
                $value['details']['password'] 		= md5($this->input->post('password'));
                $value['details']['added_date'] 	= date('Y-m-d H:i:s');
                $value['details']['added_by']		= $this->session->userdata('PRJ_USER_ID');
                $value['details']['status'] 		= 1;
                $value['details']['company']        = $this->session->userdata('PRJ_COMPANY_ID');
                $this->Usersmodel->insertuser($value['details']);

                $this->session->set_flashdata('MSG_SUC_ADD','A new user has been added with username: '.$this->input->post('username'));
                redirect('Users/ListAll');
            }
            $group = $this->Usersmodel->listgroup();
            $groupdropdown = $this->getFormhelperDropDownGroup($group);
            $data['group'] = $groupdropdown;

            $this->breadcrumb->populate(array(
                'Home' => '',
                'Users' => 'Users/ListAll',
                'Add'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['title'] = 'Add User';
            $data['pagetitle'] = 'Add New User';
            $data['page'] = 'add';
            $data['script'] = 'addscript';
            $this->load->vars($data);
            $this->load->view('main');
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password.');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }
    public function EditUser()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "ADD")) {
            if($this->input->post('Submit'))
            {
                $id                                 = $this->input->post('UserID');
                $value['details']['first_name']     = $this->input->post('fname');
                $value['details']['last_name']      = $this->input->post('lname');
                $value['details']['email']          = $this->input->post('email');
                $value['details']['phone']          = $this->input->post('phone');
                $value['details']['user_group']     = $this->input->post('role');
                $value['details']['user_name']      = $this->input->post('username');
                $value['details']['modified_date']     = date('Y-m-d H:i:s');
                $value['details']['modified_by']       = $this->session->userdata('PRJ_USER_ID');
                $value['details']['status']         = 1;
                $value['details']['company']        = $this->session->userdata('PRJ_COMPANY_ID');
                
                if($this->input->post('changepassword') == "Yes"){
                    $value['details']['password']       = md5($this->input->post('password'));
                }
                
                $this->Usersmodel->updateuser($id,$value['details']);

                $this->session->set_flashdata('MSG_SUC_ADD','User Information successfully changed for '.$this->input->post('username'));
                redirect('Users/ListAll');
            }
            $id = $this->uri->segment(3);
            $data['query'] = $this->Usersmodel->getUserData($id);

            $group = $this->Usersmodel->listgroup();
            $groupdropdown = $this->getFormhelperDropDownGroup($group);
            $data['group'] = $groupdropdown;

            $this->breadcrumb->populate(array(
                'Home' => '',
                'Users' => 'Users/ListAll',
                'Edit'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['title'] = 'Edit User';
            $data['pagetitle'] = 'Edit User';
            $data['page'] = 'edit';
            $data['script'] = 'addscript';
            $this->load->vars($data);
            $this->load->view('main');
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password.');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }
    function EditUserPerm(){
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"EDIT")){
            if($this->input->post('Submit'))
            {
                $chk_permission = $this->input->post('chk_permission');
                $login_id = $this->session->userdata('PRJ_USER_ID');
                $user_id = $this->uri->segment(3);
                $this->Usersmodel->updateuser_perm($chk_permission, $user_id, $login_id);
                $this->session->set_flashdata('success_message', 'User Permission Saved Successfully.');
                redirect('Users/ListAll', 'location');
            }
            
            $user_id = $this->uri->segment(3);
            $group_id = $this->Usersmodel->getgroupid($user_id);
            $parentmodules = $this->Usersmodel->listmodule();

            $this->breadcrumb->populate(array(
                'Home' => '',
                'Users' => 'Users/ListAll',
                'Permissions'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['group_id'] = $group_id;
            $data['parentmodules'] = $parentmodules;
            $data['pagetitle']  = 'Edit User Permission';
            $data['title']  = 'Edit User Permission';
            $data['script'] = 'addscript';
            $data['page'] = 'edituserperm';
            $this->load->vars($data);
            $this->load->view('main');
        } else {
            $this->session->set_flashdata('error', 'Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login', 'location');
        }
    }
    function getFormhelperDropDownGroup($query,$id=''){
        $datatoreturn = array('--Select--');
        $a = array('0');
        $c = array();
        if($query->num_rows()>0)
        {
            foreach ($query->result() as $row) {
                array_push($a, $row->groupid);
                array_push($datatoreturn, $row->group_name);
                $c = array_combine($a, $datatoreturn);
            }
            return $c;
        }
        else
        {
            return '';
        }
    }
    function getFormhelperDropDown($query,$id=''){
        $datatoreturn = array('--Select--');
        $a = array('0');
        $c = array();
        if($query->num_rows()>0)
        {
            foreach ($query->result() as $row) {
                array_push($a,$row->ID);
                array_push($datatoreturn,$row->Title);
                $c = array_combine($a,$datatoreturn);
            }
            return $c;
        }
        else
        {
            return '';
        }
    }
    public function GetEmployeeData()
    {
        $employeeid = $this->input->get('employee');
        $employee = $this->Usersmodel->GetEmployeeData($employeeid);
        echo json_encode($employee);
    }
}