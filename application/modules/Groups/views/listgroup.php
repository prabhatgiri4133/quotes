<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <?php echo $breadcrumb;?>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> <?php echo $pagetitle;?> &nbsp;
            <?php
            if($this->authlibrary->HasModulePermission('MANAGE-GROUP','ADD')){
                echo anchor('Groups/AddGroup','<i class="fa fa-plus"></i> Add New Group', array('class'=>'btn green pull-right'));
            }
            ?>
        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="groupslist">
                            <thead>
                            <tr class="">
                                <th> Group </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($groups->num_rows()>0){
                                foreach($groups->result() as $group){
                                    ?>
                                    <tr>
                                        <td><?php echo $group->group_name;?></td>
                                        <td width="20%"><?php if($this->authlibrary->HasModulePermission($this->module_code,"EDIT")){?>
                                                <a href="<?php echo base_url()?>Groups/EditGroup/<?php echo $group->groupid?>" class="btn blue btn-sm"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                                <a href="<?php echo base_url()?>Groups/EditGroupPerm/<?php echo $group->groupid?>" class="btn blue btn-sm"><i class="glyphicon glyphicon-certificate"></i> Permissions</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->