<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <?php echo $breadcrumb;?>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> <?php echo $pagetitle;?></h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <?php echo form_open('Groups/EditGroup', array('name'=>'AddGroup', 'id'=>'AddGroup', 'method'=>'post', 'class'=>'form-horizontal'));?>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="actions">
                            <a href="javascript:;" id="savegroup_button" class="btn green"><i class="fa fa-save"></i> Save</a>
                            <a href="javascript:;" id="cancelgroup_button" class="btn red"><i class="fa fa-remove"></i> Cancel</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="GroupName" class="control-label col-md-3">Role</label>
                                    <div class="col-md-9">
                                    <?php $group = (isset($query->group_name) && $query->group_name!='')?$query->group_name:''?>
                                        <?php echo form_input(array('name'=>'groupname', 'id'=>'FirstName', 'class'=>'form-control','value'=>$group));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $groupid = (isset($query->groupid) && $query->groupid!='')?$query->groupid:''?>
                <?php echo form_input(array('type'=>'hidden','name'=>'ID', 'id'=>'ID','value'=>$groupid, 'class'=>'form-control', 'required'=>'required'));?>
                <?php echo form_input(array('type'=>'hidden','name'=>'Submit', 'id'=>'Submit','value'=>'Submit', 'class'=>'form-control', 'required'=>'required'));?>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->