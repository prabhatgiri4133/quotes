<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/2/16
 * Time: 9:57 PM
 */
class Groups extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Groupsmodel');
        $this->module_code = 'MANAGE-GROUP';
    }

    public function Index()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "VIEW")){
            redirect('Groups/ListAll','location');
        }else{
            $this->session->set_flashdata('error','Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login','location');
        }
    }

    
    function ListAll(){
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"VIEW")){
            $query = $this->Groupsmodel->listGroup();
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Groups' => 'Groups/ListAll',
                'List All'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['pagetitle'] 	= 'Groups';
            $data['title'] 	= 'Groups';
            $data['groups'] = $query;
            $data['script'] = 'listscript';
            $data['page'] = 'listgroup';
            $this->load->vars($data);
            $this->load->view('main');
        } else {
            $this->session->set_flashdata('error', 'Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login', 'location');
        }

    }
    function AddGroup(){
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"ADD")){
            if($this->input->post('Submit'))
            {
                $value['details']['group_name'] = $this->input->post('groupname');
                $value['details']['status'] = 1;
                $value['details']['company'] = $this->session->userdata('PRJ_COMPANY_ID');
                $this->Groupsmodel->addGroup($value['details']);

                $this->session->set_flashdata('MSG_SUC_ADD','A new group has been added with name: '.$this->input->post('groupname'));
                redirect('Groups/ListAll');
            }

            $query = $this->Groupsmodel->listGroup();
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Groups' => 'Groups/ListAll',
                'Add'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['pagetitle'] 	= 'Add Group';
            $data['title'] 	= 'Add Group';
            $data['script'] = 'addscript';
            $data['page'] = 'addgroup';
            $this->load->vars($data);
            $this->load->view('main');
        } else {
            $this->session->set_flashdata('error', 'Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login', 'location');
        }

    }
    function EditGroup(){
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"ADD")){
            if($this->input->post('Submit'))
            {
                $id = $this->input->post('ID');
                $value['details']['group_name'] = $this->input->post('groupname');
                $this->Groupsmodel->editGroup($value['details'],$id);

                $this->session->set_flashdata('MSG_SUC_ADD','Group has been modified');
                redirect('Groups/ListAll');
            }
            $id = $this->uri->segment(3);
            $query = $this->Groupsmodel->getGroup($id);
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Groups' => 'Groups/ListAll',
                'Edit'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['query'] = $query;
            $data['pagetitle']  = 'Edit Group';
            $data['title']  = 'Edit Group';
            $data['script'] = 'addscript';
            $data['page'] = 'editgroup';
            $this->load->vars($data);
            $this->load->view('main');
        } else {
            $this->session->set_flashdata('error', 'Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login', 'location');
        }
    }
    function EditGroupPerm(){
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"EDIT")){
            if($this->input->post('Submit'))
            {
                $chk_permission = $this->input->post('chk_permission');
                $login_id = $this->session->userdata('PRJ_USER_ID');
                $group_id = $this->uri->segment(3);
                $this->Groupsmodel->updategroup_permision($chk_permission, $group_id, $login_id);
                $this->session->set_flashdata('success_message', 'Group Permission Saved Successfully.');
                redirect('Groups/ListAll', 'location');
            }
            $id = $this->uri->segment(3);
            
            $parentmodules = $this->Groupsmodel->listmodule();
            $qry = $this->Groupsmodel->getGroup($id);
            if(!$qry)
            {
                $this->session->set_flashdata('MSG_ERR_INVALID_DATA', 'Please use the links provided to process query.');
                redirect('Groups/ListAll', 'location');
            }
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Groups' => 'Groups/ListAll',
                'Permissions'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['parentmodules'] = $parentmodules;
            $data['pagetitle']  = 'Edit Group Permission for '.$this->Groupsmodel->getgroupname($id);;
            $data['title']  = 'Edit Group Permission';
            $data['script'] = 'addscript';
            $data['page'] = 'editgroupperm';
            $this->load->vars($data);
            $this->load->view('main');
        } else {
            $this->session->set_flashdata('error', 'Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login', 'location');
        }
    }
}