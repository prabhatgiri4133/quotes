<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <?php $this->load->view('homescript');?>

    <meta property="og:url"                     content="<?php echo base_url()?>" />
    <meta property="og:type"                    content="article" />
    <meta property="og:title"                   content="Quotes" />
    <meta property="og:description"             content="Food For Soul" />
    <meta property="og:image"                   content="<?php echo resource_url()?>img/quotes.png" />
    <meta property="fb:app_id"                   content="1716309498619545" />

</head>
<body>
    <div class="contents">
    <div class="top-left col-md-6 quarter ">
        <div class=" quote col-md-12">
           
        </div>
        <div class="author col-md-6 col-md-offset-3">
           
        </div>
    </div>
    <div class="top-right col-md-6 quarter ">
        <div class=" quote col-md-12">
           
        </div>
        <div class="author col-md-6 col-md-offset-3">
           
        </div>
    </div>
    <div class="bottom-left col-md-6 quarter ">
        <div class=" quote col-md-12">
           
        </div>
        <div class="author col-md-6 col-md-offset-3">
           
        </div>
    </div>
    <div class="bottom-right col-md-6 quarter ">
        <div class=" quote col-md-12">
           
        </div>
        <div class="author col-md-6 col-md-offset-3">
           
        </div>
    </div>
</div>
    <!-- <div class="col-md-12">
        <div class="top-left col-md-6 ">
            <div class=" quote col-md-12">
               
            </div>
            <div class="author col-md-3 col-md-offset-9">
               
            </div>
        </div>
        <div class="top-right col-md-6">
            <div class=" quote col-md-12">
               
            </div>
            <div class="author col-md-3 col-md-offset-9">
               
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="bottom-left col-md-6">
            <div class=" quote col-md-12">
               
            </div>
            <div class="author col-md-3 col-md-offset-9">
               
            </div>
        </div>
        <div class="bottom-right col-md-6">
            <div class=" quote col-md-12">
               
            </div>
            <div class="author col-md-3 col-md-offset-9">
               
            </div>
        </div>
    </div> -->
</body>
</html>