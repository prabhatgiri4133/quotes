<script type="text/javascript" src="<?php echo resource_url()?>js/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo resource_url()?>js/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo resource_url()?>js/jquery-ui/jquery-ui.structure.min.css">
<script type="text/javascript">
    get_quote();
    setInterval(get_quote, 10000);
    var count = 0;
    function display(msg)
    {
        var parent = '.bottom-left' ;
        var x = Math.floor((Math.random() * 2) + 1);
        // if(x==1)
        // {
        //     parent = '.top-left';
        // }
        // if(x==2)
        // {
        //     parent = '.top-right';
        // }
        if(x==1)
        {
            parent = '.bottom-left';
        }
        else
        {
            parent = '.bottom-right';
        }
        $(parent).find('div.quote').html('"'+msg.quote+'"').fadeIn('slow');
        $(parent).find('div.author').html('-'+msg.author).fadeIn('slow');
        $('div').animate({'color': msg.font_color});
        $('body').animate({'background-color': msg.background});
    }
    function get_quote(){
        $('.quote').fadeOut("slow");
        $('.author').fadeOut('slow');
       $.ajax({
            url: '<?php echo base_url()?>Home/get_quotes',
        })
        .done(function(msg) {
            msgs = $.parseJSON(msg);
            display(msgs);
            
        })
        .fail(function() {
            console.log("error");
        });
    }
</script>                
            
<style type="text/css">
    body{
        background-color:black;
    }
    div{
        font-family: DawningofaNewDay;
        font-size: 135%;
        color: white;
    }
    @font-face {
        font-family: DawningofaNewDay;
        src: url(<?php echo resource_url()?>fonts/DawningofaNewDay.ttf);
        font-weight: bold;
    }
    html,body {
      padding:0;
      margin:0;
      height:100%;
      min-height:100%;
     }

    .quarter{
      width:50%;
      height:80%;
      float:left;
    }
    .contents{
      height:50%;
      width:100%;
    }
</style>