<?php
class Homemodel extends CI_model
{
	function __construct()
	{
		parent::__construct();
	
	}	
	/**
	*Returns a random Quote 
	*@params void
	*@returns results
	*@created by Prabhat Giri
	*@modified by
	*/
	function get_quotes()
	{
		$this->db->select('*');
		$this->db->where('status','enabled');
		$this->db->from('quotes');
		$this->db->order_by('rand()');
		$this->db->limit(1);
		return $this->db->get();
	}

}
?>