<?php
class Home extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('homemodel');
    }

    public function Index(){
            $data['title'] = 'Quotes';
            $this->load->view('home', $data);
    }
    public function get_quotes(){
        if($this->input->is_ajax_request()){
            $quote = $this->homemodel->get_quotes();
            echo json_encode($quote->row());
        }
        else
        {
            show_404($page = '', $log_error = TRUE);
            // $this->output->set_status_header(404, 'Not Found');
            // redirect();
        }
    }
}