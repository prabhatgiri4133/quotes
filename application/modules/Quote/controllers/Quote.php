<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/2/16
 * Time: 9:57 PM
 */
class Quote extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Quotemodel');
        $this->module_code = 'MANAGE-USER';
    }

    public function Index()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "VIEW")){
            redirect('Quote/ListAll','location');
        }else{
            $this->session->set_flashdata('error','Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login','location');
        }
    }

    public function ListAll()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"VIEW")){
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Quote' => 'Quote/ListAll',
                'List All'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['Quotes'] = $this->Quotemodel->listQuotes();
            $data['title'] 	= 'Manage Quote';
            $data['pagetitle'] = 'Manage Quote';
            $data['page'] = 'list';
            $data['script'] = 'listscript';
            $this->load->vars($data);
            $this->load->view('main');
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }
    public function Detail()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code,"VIEW")){
            $id = $this->uri->segment(3);
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Quote' => 'Quote/ListAll',
                'Detail'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['Quotes'] = $this->Quotemodel->listQuoteByID($id)->row();
            $data['title']  = 'Manage Quote';
            $data['pagetitle'] = 'Manage Quote';
            $data['page'] = 'detail';
            $data['script'] = 'listscript';
            $this->load->vars($data);
            $this->load->view('main');
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }

    public function Add()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "ADD")) {
            if($this->input->post('Submit'))
            {
                
                $value['details']['quote']      	= $this->input->post('quote');
                $value['details']['author'] 		= $this->input->post('author');
                $value['details']['background'] 	= $this->input->post('background');
                $value['details']['font_color'] 	= $this->input->post('font_color');
                $value['details']['created_by']     = $this->session->userdata('PRJ_USER_ID');
                $value['details']['status'] 		= $this->input->post('status');
                $this->Quotemodel->insertquote($value['details']);

                $this->session->set_flashdata('MSG_SUC_ADD','Congratulations!!! You\'ve added a new Quote' );
                redirect('Quote/ListAll');
            }

            $this->breadcrumb->populate(array(
                'Home'  => '',
                'Quote' => 'Quote/ListAll',
                'Add'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['title']      = 'Add Quote';
            $data['pagetitle']  = 'Add Quote';
            $data['page']       = 'add';
            $data['script']     = 'addscript';
            $this->load->vars($data);
            $this->load->view('main');
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password.');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }
    public function Edit()
    {
        if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "MODIFY")) {
            if($this->input->post('Submit'))
            {
                $id                                 = $this->input->post('QuoteID');
                $value['details']['quote']          = $this->input->post('quote');
                $value['details']['author']         = $this->input->post('author');
                $value['details']['background']     = $this->input->post('background');
                $value['details']['font_color']     = $this->input->post('font_color');
                $value['details']['created_by']     = $this->session->userdata('PRJ_USER_ID');
                $value['details']['status']         = $this->input->post('status');
                
                $this->Quotemodel->updatequote($id,$value['details']);

                $this->session->set_flashdata('MSG_SUC_ADD','Successfully updated the Quote');
                redirect('Quote/ListAll');
            }
            $id = $this->uri->segment(3);
            $data['query'] = $this->Quotemodel->listQuoteById($id)->row();

            $this->breadcrumb->populate(array(
                'Home' => '',
                'Quote' => 'Quote/ListAll',
                'Edit'
            ));
            $data['breadcrumb'] = $this->breadcrumb->output();
            $data['title'] = 'Edit Quote';
            $data['pagetitle'] = 'Edit Quote';
            $data['page'] = 'edit';
            $data['script'] = 'addscript';
            // $this->load->vars($data);
            $this->load->view('main',$data);
        }else{
            $this->session->set_flashdata('MSG_ERR_INVALID_LOGIN','Please login with your username and password.');
            $this->session->set_userdata('return_url', current_url());
            redirect('Login');
        }
    }
    function delete()
    {
         if($this->authlibrary->IsLoggedIn() && $this->authlibrary->HasModulePermission($this->module_code, "DELETE")) {
            $id = $this->uri->segment(3);
            $delete = $this->Quotemodel->delete($id);
            redirect('Quote/ListAll');
        }
    }
}