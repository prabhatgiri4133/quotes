<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/2/16
 * Time: 10:03 PM
 */
class Quotemodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listQuotes(){
        $this->db->select('*');
        $this->db->from('quotes');
        $this->db->where('status !=','deleted');
        $this->db->order_by('created_on','desc');
        return $this->db->get();
    }
    public function listQuoteByID($id){
        $this->db->select('*');
        $this->db->from('quotes');
        $this->db->where('status !=','deleted');
        $this->db->where('id',$id);
        $this->db->order_by('created_on','desc');
        return $this->db->get();
    }
    function insertQuote($data)
    {
        $this->db->insert('quotes', $data);
    }
    function updatequote($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->update('quotes',$data);
    }
    function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->set('status','deleted');
        $this->db->update('quotes');
    }
}