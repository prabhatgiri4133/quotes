<section id="content">
    <div class="container">
        <div class="card">
            <div class="action-header palette-Teal-400 bg clearfix">
                <div class="ah-label hidden-xs palette-White text"><?php echo $title?></div>

                <div class="ah-search">
                    <input type="text" placeholder="Start typing..." class="ahs-input">

                    <i class="ah-search-close zmdi zmdi-long-arrow-left" data-ma-action="ah-search-close"></i>
                </div>

                <ul class="ah-actions actions a-alt">
                    <li>
                        <div class="portlet-title pull-right">
                                <a href="<?php echo base_url();?>Quote/Add" class="btn btn-primary waves-effect"><i class="fa fa-add"></i> Add New</a>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="list-group lg-alt lg-even-black">
                <div class="list-group-item media">
                    <div class="media-body">
                       <div class="table-responsive">
                            <table id="data-table-basic" class="table table-vmiddle table-condensed table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th data-column-id="name"> Quote </th>
                                        <th data-column-id="Quotename"> Author </th>
                                        <th data-column-id="group" data-order="desc"> Background Color </th>
                                        <th data-column-id="last-login"> Font Color </th>
                                        <th data-column-id="created"> Created On </th>
                                        <th data-column-id="created"> Status </th>
                                        <th>Link</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($Quotes->num_rows()>0){
                                        foreach($Quotes->result() as $Quote){
                                            ?>
                                            <tr>
                                                <td><?php echo anchor('Quote/Detail/'.$Quote->id,$Quote->quote);?></td>
                                                <td><?php echo $Quote->author;?></td>
                                                <td><?php echo $Quote->background;?></td>
                                                <td><?php echo $Quote->font_color;?></td>
                                                <td><?php echo $Quote->created_on?></td>
                                                <td><?php echo $Quote->status?></td>
                                                <td><a href="<?php echo base_url()?>Quote/Edit/<?php echo $Quote->id?>">Edit   </a> <a href="<?php echo base_url()?>Quote/Delete/<?php echo $Quote->id?>">Delete</a>  </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                            </table>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
</section>