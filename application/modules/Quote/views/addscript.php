<link href="<?php echo resource_url()?>vendors/summernote/dist/summernote.css" rel="stylesheet">
<script src="<?php echo resource_url()?>vendors/summernote/dist/summernote-updated.min.js"></script>


<script type="text/javascript">
    $('#save_button').on('click', function () {
        $('#Submit').click();
    });
    $('#cancel_button').on('click', function () {
        window.location.assign('<?php echo base_url()?>Users/ListAll');
    });
</script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<script src="<?php echo resource_url()?>vendors/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?php echo resource_url()?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<link href="<?php echo resource_url()?>vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
<script src="<?php echo resource_url()?>vendors/bootgrid/jquery.bootgrid.updated.min.js"></script>

<script type="text/javascript">
 $(document).ready(function(){
    //Basic Example
    // $("#data-table-basic").bootgrid({
    //     css: {
    //         icon: 'zmdi icon',
    //         iconColumns: 'zmdi-view-module',
    //         iconDown: 'zmdi-expand-more',
    //         iconRefresh: 'zmdi-refresh',
    //         iconUp: 'zmdi-expand-less'
    //     },
    // });
  
});


var UIAlertsApi = function () {

    var handleDemo = function() {
        <?php
        if($this->session->flashdata('MSG_SUC_ADD') != ''){
        ?>
            App.alert({
                //container: $('#alert_container').val(), // alerts parent container(by default placed after the page breadcrumbs)
                //place: $('#alert_place').val(), // append or prepent in container
                type: 'success',  // alert's type
                message: '<?php echo $this->session->flashdata('MSG_SUC_ADD');?>',  // alert's message
                //close: $('#alert_close').is(":checked"), // make alert closable
                //reset: $('#alert_reset').is(":checked"), // close all previouse alerts first
                //focus: $('#alert_focus').is(":checked"), // auto scroll to the alert after shown
                //closeInSeconds: $('#alert_close_in_seconds').val(), // auto close after defined seconds
                //icon: $('#alert_icon').val() // put icon before the message
            });
        <?php }
        if($this->session->flashdata('MSG_ERR_INVALID_DATA') != ''){
            ?>
            App.alert({
                //container: $('#alert_container').val(), // alerts parent container(by default placed after the page breadcrumbs)
                //place: $('#alert_place').val(), // append or prepent in container
                type: 'warning',  // alert's type
                message: '<?php echo $this->session->flashdata('MSG_ERR_INVALID_DATA');?>',  // alert's message
                //close: $('#alert_close').is(":checked"), // make alert closable
                //reset: $('#alert_reset').is(":checked"), // close all previouse alerts first
                //focus: $('#alert_focus').is(":checked"), // auto scroll to the alert after shown
                //closeInSeconds: $('#alert_close_in_seconds').val(), // auto close after defined seconds
                //icon: $('#alert_icon').val() // put icon before the message
            });
            <?php
        }
        ?>
    }

    return {

        //main function to initiate the module
        init: function () {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {
    UIAlertsApi.init();
});

</script>