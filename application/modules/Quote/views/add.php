<!-- BEGIN CONTENT -->



<section id="content">
    <div class="container">
        <div class="card">
            <div class="action-header palette-Teal-400 bg clearfix">
                <div class="ah-label hidden-xs palette-White text"><?php echo $title?></div>
            </div>

            <div class="list-group lg-alt lg-even-black">
                <div class="list-group-item media">
                    <div class="media-body">
                      <div class="col-md-12">
                            <?php echo form_open('Quote/Add', array('name'=>'', 'id'=>'', 'method'=>'post', 'class'=>'form-horizontal'));?>
                            <div class="portlet light bordered">
                                <div class="portlet-title pull-right">
                                    <div class="actions">
                                        <a href="#" class="btn btn-primary waves-effect" id="save_button"><i class="fa fa-edit"></i> Save</a>
                                        <a href="<?php echo base_url();?>Quote/ListAll" class="btn btn-danger waves-effect"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="status" class="control-label col-md-3">Status</label>
                                                <div class="col-md-9">
                                                    <?php echo form_dropdown('status', array('enabled'=>'Enabled','disabled'=>'Disabled'),'',array('id'=>'UserRole', 'class'=>'bs-select form-control', 'required'=>'required'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="font_color" class="control-label col-md-3">Font Color</label>
                                                <div class="col-md-9">
                                                    <?php echo form_input(array('name'=>'font_color', 'id'=>'font_color', 'class'=>'form-control', 'required'=>'required'));?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="background" class="control-label col-md-3">Background Color</label>
                                                <div class="col-md-9">
                                                    <?php echo form_input(array('name'=>'background', 'id'=>'background', 'class'=>'form-control'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="author" class="control-label col-md-3">Author</label>
                                                <div class="col-md-9">
                                                    <?php echo form_input(array('name'=>'author', 'type'=>'text', 'id'=>'author', 'class'=>'form-control', 'required'=>'required'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="quote" class="control-label col-md-1">Quote</label>
                                                <div class="col-md-9 note-editor panel panel-default">
                                                    <?php echo form_textarea(array('name'=>'quote', 'id'=>'quote', 'class'=>'form-control', 'required'=>'required'));?>
                                                    <?php echo form_input(array('type'=>'submit','name'=>'Submit', 'id'=>'Submit','value'=>'Submit', 'class'=>'form-control hidden', 'required'=>'required'));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CONTENT -->