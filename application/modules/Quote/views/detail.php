<!-- BEGIN CONTENT -->



<section id="content">
    <div class="container">
        <div class="card">
            <div class="action-header palette-Teal-400 bg clearfix">
                <div class="ah-label hidden-xs palette-White text"><?php echo $title?></div>
            </div>

            <div class="list-group lg-alt lg-even-black">
                <div class="list-group-item media">
                    <div class="media-body">
                      <div class="col-md-12">
                            <?php echo form_open('Quote/', array('name'=>'', 'id'=>'', 'method'=>'post', 'class'=>'form-horizontal'));?>
                            <div class="portlet light bordered">
                                <div class="portlet-title pull-right">
                                    <div class="actions">
                                        <a href="<?php echo base_url();?>Quote/Edit/<?php echo $Quotes->id;?>" class="btn btn-primary waves-effect"><i class="fa fa-edit"></i> Edit</a>
                                        <a href="<?php echo base_url();?>Quote/ListAll" class="btn btn-danger waves-effect"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="UserRole" class="control-label col-md-3 bold">Author</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo $Quotes->author;?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="FirstName" class="control-label col-md-3 bold">Quote</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo $Quotes->quote?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="Email" class="control-label col-md-3 bold">Font Color</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo (isset($Quotes->font_color) && $Quotes->font_color!='')?$Quotes->font_color:''?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="Phone" class="control-label col-md-3 bold">Background Color</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo (isset($Quotes->background) && $Quotes->background!='')?$Quotes->background:''?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CONTENT -->