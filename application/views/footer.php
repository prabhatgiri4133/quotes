<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
			
		<footer id="footer">
                Copyright &copy; <?php echo date('Y')?> Quotes

                <ul class="f-menu">
                    <li><a href="<?php echo resource_url()?>#">Home</a></li>
                    <!-- <li><a href="<?php echo resource_url()?>#">Dashboard</a></li>
                    <li><a href="<?php echo resource_url()?>#">Reports</a></li>
                    <li><a href="<?php echo resource_url()?>#">Support</a></li>
                    <li><a href="<?php echo resource_url()?>#">Contact</a></li> -->
                </ul>
            </footer>

        </section>

        <!-- Page Loader -->
        <div class="page-loader palette-Teal bg">
            <div class="preloader pl-xl pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                </svg>
            </div>
        </div>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="<?php echo resource_url()?>http://www.google.com/chrome/">
                                <img src="<?php echo resource_url()?>img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo resource_url()?>https://www.mozilla.org/en-US/firefox/new/">
                                <img src="<?php echo resource_url()?>img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo resource_url()?>http://www.opera.com">
                                <img src="<?php echo resource_url()?>img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo resource_url()?>https://www.apple.com/safari/">
                                <img src="<?php echo resource_url()?>img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo resource_url()?>http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="<?php echo resource_url()?>img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->

        <!-- Javascript Libraries -->
        <script src="<?php echo resource_url()?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/salvattore/dist/salvattore.min.js"></script>

        <script src="<?php echo resource_url()?>vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="<?php echo resource_url()?>vendors/sparklines/jquery.sparkline.min.js"></script>
        <script src="<?php echo resource_url()?>vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="<?php echo resource_url()?>js/flot-charts/curved-line-chart.js"></script>
        <script src="<?php echo resource_url()?>js/flot-charts/line-chart.js"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
        <script src="<?php echo resource_url()?>vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="<?php echo resource_url()?>js/charts.js"></script>

        <script src="<?php echo resource_url()?>js/functions.js"></script>
        <script src="<?php echo resource_url()?>js/actions.js"></script>
        <script src="<?php echo resource_url()?>js/demo.js"></script>

        <?php $this->output->enable_profiler(true);?>

    </body>
  
<!-- Mirrored from byrushan.com/projects/mae/1-0/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Aug 2017 04:12:06 GMT -->
</html>