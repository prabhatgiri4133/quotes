<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	$this->load->view('header');
	$this->load->view($page);
	($script!='')?$this->load->view($script):'';
	$this->load->view('footer');
	
?>